;;;; Exercice 3 : lambda l'ultime

#lang racket

;;((lambda (x y z) (+ x y z)) 1 2 3)

(define zero (lambda (f) (lambda (x) x)))
(define un (lambda (f) (lambda (x) (f x))))
(define deux (lambda (f) (lambda (x) (f (f x)))))

(define (num->str n)
  ((n (lambda (s) (string-append "." s))) ""))

(define (num->int n)
  ((n (lambda (x) (+ x 1))) 0))

;;; Q1

(define (num n)
  (if (zero? n) zero
      (lambda (f) (lambda (x) (((num (- n 1)) f) (f x))))))

;; (define (num n)
;;   (if (zero? n) zero
;;       (lambda (f) (lambda (x) (f (((num (- n 1)) f) x))))))

;;; Q2

(define (succ n)
  (lambda (f) (lambda (x) ((n f) (f x)))))

(define (num2 n)
  (if (zero? n) zero
      (succ (num2 (- n 1)))))

;;; Q3

;; Peano :
;; n = O | S n
;; plus 0 n = n
;; plus (S m) n = S (plus m n)

(define (plus m n)
  (lambda (f) (lambda (x) ((m f) ((n f) x)))))

(define (plus2 m n)
  ((m succ) n))

;;; Q4

(define (mult m n)
  ((m (lambda (x) (plus x n))) zero))

;;;; Exercice 4 : TME à rendre

;;;; Exercice 5 : Challenge : lambda l'ultime : arbres

;;; Q1

;;; Q2

;;; Q3

;;; Q4

;;; Q5

;;; Q6
