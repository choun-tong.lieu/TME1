;;;; Exercice 2 : Fibonacci en recursion (terminale)

#lang racket

;;; Q1

(define (fib n)
  (cond
    ((zero? n) 0)
    ((= n 1) 1)
    (else (+ (fib (- n 1)) (fib (- n 2))))))

;;; Q2
;;; Voir fib.c

;;; Q3

(define (fibit n a b)
  (if (zero? n) a
      (fibit (- n 1) b (+ a b))))

;;; Q4 : factorielle recursive terminale ?

(define (facttail n acc)
  (if (or (zero? n) (= n 1)) acc
      (facttail (- n 1) (* n acc))))
