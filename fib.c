
int fibonacci(int n) {
  int a = 0;
  int b = 1;
  while(n-- >= 1){
    int temp = b;
    b = a + b;
    a = temp;
  }
  return a;
}
