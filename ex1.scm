;;;; Exercice 1 : mise en route

#lang racket

;;; Q1

(define (perimetre lng lrg)
  (* 2 (+ lng lrg)))

;;; Q2

(define (aire-triangle a b c)
  (let ((p (/ (+ a b c) 2)))
    (sqrt (* p (- p a) (- p b) (- p c)))))
